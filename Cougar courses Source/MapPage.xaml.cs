﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CS481_HW6
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        ObservableCollection<Pin> pinNames;
		public MapPage ()
		{
			InitializeComponent ();
            MainMap.MapType = MapType.Street;
            //Sets the initial map load location
            var initialMapLocation = MapSpan.FromCenterAndRadius
                                                (new Position(33.1307785, -117.1601826)
                                                 , Distance.FromMiles(1));

            MainMap.MoveToRegion(initialMapLocation);

            PopulatePicker();
        }
        private void PopulatePicker()
        {
            //Instantiate five pins for the map
            pinNames = new ObservableCollection<Pin>
            {
                new Pin
                {
                    Type =PinType.SavedPin, Label="CSUSM", Address="333 N Twin Oaks Valley Rd",
                    Position = new Position(33.12776495628767, -117.15794239864198)
                },
                new Pin
                {
                    Type =PinType.SavedPin, Label="Habit Grill", Address="727 W San Marcos Blvd",
                    Position = new Position(33.13616544192321, -117.17896018848268)
                },
                new Pin
                {
                    Type=PinType.SavedPin, Label="Fry's", Address="150 S Bent Ave",
                    Position = new Position(33.1372354, -117.1836399)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="URGE Pub", Address="255 Redel Rd",
                    Position = new Position(33.1358939, -117.15856439999999)
                },
                new Pin
                {
                    Type = PinType.SavedPin, Label="Movie Theater", Address="1180 W San Marcos Blvd",
                    Position = new Position(33.13507480000001, -117.1928289)
                }
            };

            PinPicker.Items.Add("---");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                PinPicker.Items.Add(item.Label);
                MainMap.Pins.Add(item);
            }

            PinPicker.SelectedIndex = 0;
            
        }

        private void Handle_SelectedPinChange(object sender, System.EventArgs e)
        {
            var inputSlot = (Picker)sender;
            string pname = inputSlot.SelectedItem.ToString();
            //When user selects a pin name, find the pin
            foreach(Pin item in pinNames)
            {
                if(item.Label == pname)
                {
                    //Move the map to center on the pin
                    MainMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                }
            }
            
        }
        private void Handle_SatButton(object sender, System.EventArgs e)
        {
            //Switch the map to Satellite View
            MainMap.MapType = MapType.Satellite;
        }
        private void Handle_StrtButton(object sender, System.EventArgs e)
        {
            //Switch the map to Street View
            MainMap.MapType = MapType.Street;
        }
        private void Handle_HybridButton(object sender, System.EventArgs e)
        {
            //Switch the map to Hybrid View
            MainMap.MapType = MapType.Hybrid;
        }
    }
}